<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Migration_hit extends CI_Migration {

  public function up () {
    $this->db->query("DROP TABLE IF EXISTS `hit`");
    $this->db->query("
      CREATE TABLE `hit` (
        `uuid` varchar(255) NOT NULL,
        `created` datetime NOT NULL,
        `shortlink` varchar(255) NOT NULL,
        `country` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
  }

  public function down () {
    $this->db->query("DROP TABLE `hit`");
  }

}