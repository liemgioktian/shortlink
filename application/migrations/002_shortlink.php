<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Migration_shortlink extends CI_Migration {

  public function up () {
    $this->db->query("DROP TABLE IF EXISTS `shortlink`");
    $this->db->query("
      CREATE TABLE `shortlink` (
        `uuid` varchar(255) NOT NULL,
        `created` datetime NOT NULL,
        `user` varchar(255) NOT NULL,
        `origin` varchar(255) NOT NULL,
        `shortlink` varchar(255) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
  }

  public function down () {
    $this->db->query("DROP TABLE `shortlink`");
  }

}