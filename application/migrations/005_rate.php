<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Migration_rate extends CI_Migration {

  public function up () {
    $this->db->query("DROP TABLE IF EXISTS `rate`");
    $this->db->query("
      CREATE TABLE `rate` (
        `uuid` varchar(255) NOT NULL,
        `created` datetime NOT NULL,
        `country` varchar(255) NOT NULL,
        `ip` varchar(255) NOT NULL,
        `rate` float NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
  }

  public function down () {
    $this->db->query("DROP TABLE `rate`");
  }

}