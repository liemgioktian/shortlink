<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Migration_withdraw extends CI_Migration {

  public function up () {
    $this->db->query("DROP TABLE IF EXISTS `withdraw`");
    $this->db->query("
      CREATE TABLE `withdraw` (
        `uuid` varchar(255) NOT NULL,
        `created` datetime NOT NULL,
        `user` varchar(255) NOT NULL,
        `amount` int(11) NOT NULL,
        `status` tinyint(1) NOT NULL DEFAULT '0',
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
  }

  public function down () {
    $this->db->query("DROP TABLE `withdraw`");
  }

}