<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Migration_user extends CI_Migration {

  public function up () {
    $this->db->query("DROP TABLE IF EXISTS `user`");
    $this->db->query("
      CREATE TABLE `user` (
        `uuid` varchar(255) NOT NULL,
        `created` datetime NOT NULL,
        `name` varchar(255) NOT NULL,
        `email` varchar(255) NOT NULL,
        `password` varchar(255) NOT NULL,
        `web` varchar(255) NOT NULL,
        `hitstat` varchar(255) NOT NULL,
        `status` tinyint(4) NOT NULL,
        `role` tinyint(4) NOT NULL,
        PRIMARY KEY (`uuid`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ");
    $this->db->query("
      INSERT INTO `user` (`uuid`, `created`, `name`, `email`, `password`, `web`, `hitstat`, `status`, `role`)
      VALUES
        ('bc7db7b4-eedb-11e7-b014-890991bdfdbd', '2018-01-01 17:08:38', 'Admin', 'admin@local.host', '202cb962ac59075b964b07152d234b70', '', '', 1, 1)
    ");
  }

  public function down () {
    $this->db->query("DROP TABLE `user`");
  }

}