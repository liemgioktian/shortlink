<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class shortlinks extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'shortlink';
    $this->form  = array ();
    $this->form[]= array(
      'name'    => 'origin',
      'label'   => 'Link',
    );
  }

  function create ($data) {
    $data['user'] = $this->session->userdata('uuid');
    return parent::create($data);
  }

  function admin ($controller) {
    $this->db->select('shortlink.*');
    $this->db->select('count(hit.uuid) hit', false);
    $this->db->join('hit', 'shortlink.uuid = hit.shortlink', 'left');
    $this->db->group_by('shortlink.uuid');

    $criteria = array();
    $this->db->select('user.name', false);
    $this->db->join('user', 'user.uuid = shortlink.user', 'left');

    $records = $this->find($criteria);
    $dtopt = new stdClass();

    foreach ($records as &$record) {
      $record->action = "
        <a href='".site_url("$controller/read/$record->uuid")."' class='btn btn-warning btn-xs'>edit</a>
        <a href='".site_url("$controller/delete/$record->uuid")."' class='btn btn-danger btn-xs'>delete</a>
      ";
      unset($record->uuid);
      unset($record->created);
      unset($record->user);
    }
    $dtopt->aaData = $records;

    $dtopt->aoColumns = array();
    if (isset ($records[0])) foreach ($records[0] as $field => $value) {
      $theadObj = new stdClass();
      $theadObj->sTitle  = strtoupper($field);
      $theadObj->mData   = $field;
      $dtopt->aoColumns[]= $theadObj;
    }

    return json_encode($dtopt);
  }

  function datatables ($controller) {
    $this->db->select('shortlink.*');
    $this->db->select('count(hit.uuid) hit', false);
    $this->db->join('hit', 'shortlink.uuid = hit.shortlink', 'left');
    $this->db->group_by('shortlink.uuid');

    $criteria = array();
    $criteria['user'] = $this->session->userdata('uuid');

    $records = $this->find($criteria);
    $dtopt = new stdClass();

    foreach ($records as &$record) {
      $record->action = "
        <a href='".site_url("$controller/read/$record->uuid")."' class='btn btn-warning btn-xs'>edit</a>
        <a href='".site_url("$controller/delete/$record->uuid")."' class='btn btn-danger btn-xs'>delete</a>
      ";
      unset($record->uuid);
      unset($record->created);
      unset($record->user);
    }
    $dtopt->aaData = $records;

    $dtopt->aoColumns = array();
    if (isset ($records[0])) foreach ($records[0] as $field => $value) {
      $theadObj = new stdClass();
      $theadObj->sTitle  = strtoupper($field);
      $theadObj->mData   = $field;
      $dtopt->aoColumns[]= $theadObj;
    }

    return json_encode($dtopt);
  }

}