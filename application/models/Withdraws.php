<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Withdraws extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'withdraw';
    $this->form  = array ();
    $this->form[]= array(
      'name'    => 'amount',
      'label'   => 'Amount',
    );
  }

  function create ($data) {
    $data['user'] = $this->session->userdata('uuid');
    return parent::create($data);
  }

  function admin ($controller) {
    $criteria = array();

    $this->db->select('withdraw.*');
    $this->db->select('user.name', false);
    $this->db->join('user', 'user.uuid = withdraw.user', 'left');

    $records = $this->find($criteria);
    $dtopt = new stdClass();

    setlocale(LC_MONETARY, 'en_US');
    foreach ($records as &$record) {
      $record->action = "
        <a href='".site_url("$controller/read/$record->uuid")."' class='btn btn-warning btn-xs'>edit</a>
        <a href='".site_url("$controller/delete/$record->uuid")."' class='btn btn-danger btn-xs'>delete</a>
      ";
      if ('1' === $record->status) $record->status = '<span class="label label-success">approved</span>';
      else if ('0' === $record->status) {
        $record->status = '<span class="label label-warning">under review</span>';
        $record->action .= "<a href='".site_url("$controller/approve/$record->uuid")."' class='btn btn-success btn-xs'>approve</a>";
      }
      $record->amount = money_format('%i', $record->amount);
      unset($record->uuid);
      unset($record->user);
    }
    $dtopt->aaData = $records;

    $dtopt->aoColumns = array();
    if (isset ($records[0])) foreach ($records[0] as $field => $value) {
      $theadObj = new stdClass();
      $theadObj->sTitle  = strtoupper($field);
      $theadObj->mData   = $field;
      $dtopt->aoColumns[]= $theadObj;
    }

    return json_encode($dtopt);    
  }

  function datatables ($controller) {
    $criteria = array();

    $criteria['user'] = $this->session->userdata('uuid');

    $records = $this->find($criteria);
    $dtopt = new stdClass();

    setlocale(LC_MONETARY, 'en_US');
    foreach ($records as &$record) {
      $record->action = '1' === $record->status ? '' : "
        <a href='".site_url("$controller/read/$record->uuid")."' class='btn btn-warning btn-xs'>edit</a>
        <a href='".site_url("$controller/delete/$record->uuid")."' class='btn btn-danger btn-xs'>delete</a>
      ";
      if ('1' === $record->status) $record->status = '<span class="label label-success">approved</span>';
      else if ('0' === $record->status) $record->status = '<span class="label label-warning">under review</span>';
      $record->amount = money_format('%i', $record->amount);
      unset($record->uuid);
      unset($record->user);
    }
    $dtopt->aaData = $records;

    $dtopt->aoColumns = array();
    if (isset ($records[0])) foreach ($records[0] as $field => $value) {
      $theadObj = new stdClass();
      $theadObj->sTitle  = strtoupper($field);
      $theadObj->mData   = $field;
      $dtopt->aoColumns[]= $theadObj;
    }

    return json_encode($dtopt);
  }

  function approve ($uuid) {
    return $this->db->where('uuid', $uuid)->set('status', 1)->update('withdraw');
  }

}