<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'user';
    $this->form  = array ();
    $this->form[]= array(
      'name'    => 'name',
      'label'   => 'Name',
      'icon'    => 'user'
    );
    $this->form[]= array(
      'name'    => 'email',
      'label'   => 'Email',
      'icon'    => 'envelope'
    );
    $this->form[]= array(
      'name'    => 'password',
      'label'   => 'Password',
      'type'    => 'password',
      'icon'    => 'lock'
    );
    $this->form[]= array(
      'name'    => 'repassword',
      'label'   => 'Confirm Password',
      'type'    => 'password',
      'icon'    => 'lock'
    );
    $this->form[]= array(
      'name'    => 'web',
      'label'   => 'Website',
      'icon'    => 'globe'
    );
    $this->form[]= array(
      'name'    => 'hitstat',
      'label'   => 'Hitstat URL',
      'icon'    => 'bar-chart'
    );
  }

  function create ($data) {
    unset($data['repassword']);
    $data['password'] = md5($data['password']);
    return parent::create($data);
  }

  function update ($data) {
    unset($data['repassword']);
    if (strlen($data['password']) > 0) $data['password'] = md5($data['password']);
    return parent::update($data);
  }

  function prepopulate ($uuid) {
    $record = $this->findOne($uuid);
    foreach ($this->form as &$f) $f['value'] = in_array($f['name'], array('password', 'repassword')) ? '' : $record[$f['name']];
    return $this->form;
  }

  function datatables ($controller) {
    $records = $this->find();
    $dtopt = new stdClass();

    foreach ($records as &$record) {
      $record->action = "
        <a href='".site_url("$controller/read/$record->uuid")."' class='btn btn-warning btn-xs'>edit</a>
        <a href='".site_url("$controller/delete/$record->uuid")."' class='btn btn-danger btn-xs'>delete</a>
      ";
      switch ($record->status) {
        case '0':
          $record->status = 'under review';
          $record->action .= '<a href="'.site_url('user/approve/' . $record->uuid ).'" class="btn btn-success btn-xs">approve</a>';
          break;
        case '1':
          $record->status = 'active';
          $record->action .= '<a href="'.site_url('user/banned/' . $record->uuid ).'" class="btn btn-danger btn-xs">banned</a>';
          break;
        case '2':
          $record->status = 'banned';
          $record->action .= '<a href="'.site_url('user/approve/' . $record->uuid ).'" class="btn btn-success btn-xs">approve</a>';
          break;
      }
      $record->role = '1' === $record->role ? 'admin' : 'member';
      unset($record->password);
      unset($record->uuid);
    }
    $dtopt->aaData = $records;

    $dtopt->aoColumns = array();
    if (isset ($records[0])) foreach ($records[0] as $field => $value) {
      $theadObj = new stdClass();
      $theadObj->sTitle  = strtoupper($field);
      $theadObj->mData   = $field;
      $dtopt->aoColumns[]= $theadObj;
    }

    $dtopt->order = array(array(0, 'desc'));
    return json_encode($dtopt);
  }

  function approve ($uuid) {
    return $this->db->where('uuid', $uuid)->set('status', 1)->update('user');
  }

  function banned ($uuid) {
    return $this->db->where('uuid', $uuid)->set('status', 2)->update('user');
  }

}