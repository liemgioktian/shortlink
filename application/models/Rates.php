<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class rates extends MY_Model {

  function __construct () {
    parent::__construct();
    $this->table = 'rate';
    $this->form  = array ();
    $this->form[]= array(
      'name'    => 'country',
      'label'   => 'Country',
    );
    $this->form[]= array(
      'name'    => 'ip',
      'label'   => 'IP Address',
    );
    $this->form[]= array(
      'name'    => 'rate',
      'label'   => 'Rate',
    );
  }

}