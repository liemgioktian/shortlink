<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

  var $loggedin     = false;
  var $controller   = '';
  var $model        = '';

  function __construct () {
    parent::__construct();
    $data = array ();
    if (!$this->session->userdata('uuid')) redirect (base_url());
  }

  public function loadview ($view, $data = array()) {
    $data['menu'] = $this->menu();
    $this->load->view($view, $data);
  }

  private function menu () {
    $menu = array();
    if ('1' === $this->session->userdata('role')) {
      $menu[]   = array (
        'url'   => site_url('user/index'),
        'title' => 'Users',
        'icon'  => 'users',
      );      
      $menu[]   = array (
        'url'   => site_url('rate/index'),
        'title' => 'Rate',
        'icon'  => 'globe',
      );
    }
    $menu[]   = array (
      'url'   => site_url('shortlink/index'),
      'title' => 'Shortlink',
      'icon'  => 'link',
    );
    $menu[]   = array (
      'url'   => site_url('withdraw/index'),
      'title' => 'Invoice',
      'icon'  => 'money',
    );
    return $menu;
  }

  public function index () {
    $data = array();
    $data['pagetype'] = 'table';

    $model = $this->model;
    $controller = $this->controller;

    $this->load->model($model);

    if ('delete' === $this->input->post('action')) $this->$model->delete($this->input->post('uuid'));
    else if ($this->input->post()) $this->$model->save($this->input->post());

    $data['dtopt'] = $this->$model->datatables($this->controller);
    $data['create']= site_url("$controller/create");
    $this->loadview('admin', $data);
  }

  function read ($id) {
    $data                 = array();
    $data['pagetype']     = 'form';
    $model                = $this->model;
    $this->load->model($model);
    $data['form']         = $this->$model->getForm($id);
    $data['listPage']     = site_url($this->controller);
    $this->loadview('admin', $data);
  }

  function create () {
    $data             = array();
    $data['pagetype'] = 'form';
    $model            = $this->model;
    $this->load->model($model);
    $data['listPage'] = site_url($this->controller);
    $data['form']     = $this->$model->getForm();
    $this->loadview('admin', $data);
  }

  function delete ($uuid) {
    $data             = array();
    $data['pagetype'] = 'confirm';
    $data['listPage'] = site_url($this->controller);
    $data['uuid']     = $uuid;
    $data['action']   = 'delete';
    $this->loadview('admin', $data);
  }

}