<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

  function __construct () {
    parent::__construct();
    $this->load->database();
  }

  function save ($record) {
    return isset ($record['uuid']) ? $this->update($record) : $this->create($record);
  }

  function create ($record) {
    $this->db->set('uuid', 'UUID()', FALSE);
    date_default_timezone_set('asia/jakarta');
    $record['created'] = date('Y-m-d H:i:s');
    $this->db->insert($this->table, $record);
    return $this->db->insert_id ();
  }

  function update ($record) {
    $this->db->where('uuid', $record['uuid'])->update($this->table, $record);
    return $record['uuid'];
  }

  function findOne ($param) {
    if (!is_array($param)) $param = array('uuid' => $param);
    return $this->db->get_where($this->table, $param)->row_array();
  }

  function find ($param = array()) {
    return $this->db->get_where($this->table, $param)->result();
  }

  function delete ($uuid) {
    return $this->db->where('uuid', $uuid)->delete($this->table);
  }

  function getForm ($uuid = false) {
    $form = $uuid ? $this->prepopulate($uuid) : $this->form;

    if ($uuid) $form[] = array(
      'name' => 'uuid',
      'type' => 'hidden',
      'value'=> $uuid,
      'label'=> 'UUID'
    );

    foreach ($form as &$f) {
      if (isset ($f['options'])) $f['type'] = 'select';
      if (!isset ($f['type'])) $f['type']   = 'text';

      if (!isset ($f['value'])) $f['value']       = '';

      if (!isset ($f['required'])) $f['required'] = '';
      else $f['required'] = 'required="required"';

      $f['disabled'] = !isset($f['disabled']) ? '' : 'disabled="disabled"';
    }
    return $form;
  }

  function prepopulate ($uuid) {
    $record = $this->findOne($uuid);
    foreach ($this->form as &$f) $f['value'] = $record[$f['name']];
    return $this->form;
  }

  function datatables ($controller) {
    $records = $this->find();
    $dtopt = new stdClass();

    foreach ($records as &$record) {
      $record->action = "
        <a href='".site_url("$controller/read/$record->uuid")."' class='btn btn-warning btn-xs'>edit</a>
        <a href='".site_url("$controller/delete/$record->uuid")."' class='btn btn-danger btn-xs'>delete</a>
      ";
      unset($record->uuid);
      unset($record->created);
    }
    $dtopt->aaData = $records;

    $dtopt->aoColumns = array();
    if (isset ($records[0])) foreach ($records[0] as $field => $value) {
      $theadObj = new stdClass();
      $theadObj->sTitle  = strtoupper($field);
      $theadObj->mData   = $field;
      $dtopt->aoColumns[]= $theadObj;
    }

    return json_encode($dtopt);
  }

}