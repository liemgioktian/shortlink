<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shortlink extends MY_Controller {

  function __construct () {
    parent:: __construct();
    $this->controller = 'shortlink';
    $this->model = 'shortlinks';
  }

  public function index () {
    $data = array();
    $data['pagetype'] = 'table';

    $model = $this->model;
    $controller = $this->controller;

    $this->load->model($model);

    if ('delete' === $this->input->post('action')) $this->$model->delete($this->input->post('uuid'));
    else if ($this->input->post()) $this->$model->save($this->input->post());

    $data['dtopt'] = 
      '1' === $this->session->userdata('role') ?
      $this->$model->admin($this->controller):
      $this->$model->datatables($this->controller);
    $data['create']= site_url("$controller/create");
    $this->loadview('admin', $data);
  }

}