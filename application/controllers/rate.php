<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rate extends MY_Controller {

  function __construct () {
    parent:: __construct();
    $this->controller = 'rate';
    $this->model = 'rates';
  }

}