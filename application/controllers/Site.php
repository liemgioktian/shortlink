<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

  function __construct () {
    parent:: __construct();
  }

  public function index () {
    $this->load->model('users');
    $data = array();

    if ($this->input->post('repassword')) {

      if ($this->input->post('password') !== $this->input->post('repassword')) {
        $this->session->set_flashdata('notification', array('alert' => 'danger', 'type' => 'Error', 'message' => 'Password not match'));
        redirect (site_url ('site/register'));
      }        

      else if (count ($this->users->find(array('email' => $this->input->post('email')))) > 0) {
        $this->session->set_flashdata('notification', array('alert' => 'danger', 'type' => 'Error', 'message' => 'Email already registered'));
        redirect (site_url ('site/register'));
      }

      $this->users->save($this->input->post());
      $data['notification'] = array('alert' => 'success', 'type' => 'Congratulations', 'message' => 'Your site is Under Review you will get Email Soon');

    } else if ($this->input->post('email')) {
      $user = $this->users->findOne(array(
        'email' => $this->input->post('email'),
        'password' => md5($this->input->post('password')),
      ));

      if (!isset ($user['uuid'])) $data['notification'] = array('alert' => 'danger', 'type' => 'Error', 'message' => 'Wrong email and password combination');
      else if ('0' === $user['status']) $data['notification'] = array('alert' => 'danger', 'type' => 'Error', 'message' => 'Your Account is Under Review Please wait few days');
      else if ('2' === $user['status']) $data['notification'] = array('alert' => 'danger', 'type' => 'Error', 'message' => 'Your User Has been banned Please contact Admin for Review complain@nyxita.com');
      else {
        $this->session->set_userdata($user);
        redirect(site_url('shortlink'));
      }
    }

    $this->session->sess_destroy();
    $this->load->view('login', $data);
  }

  public function register () {
    $data = array();
    $this->load->model('users');
    $data['fields'] = $this->users->getForm();
    if ($this->session->flashdata('notification')) $data['notification'] = $this->session->flashdata('notification');
    $this->load->view('register', $data);
  }
}