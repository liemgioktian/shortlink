<a href="<?= $create ?>" class="btn btn-info pull-right">+ Add New</a>
<br class="clearfix"><br class="clearfix">
<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
</table>
<script src="<?= base_url() ?>js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?= base_url() ?>js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?= base_url() ?>js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
  var dtopt = <?= $dtopt ?>;
  if (dtopt.aaData.length > 0) $('table').DataTable(dtopt)
  else $('table').remove()
</script>