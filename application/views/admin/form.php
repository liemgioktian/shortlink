<form class="form-horizontal" method="POST" action="<?= $listPage ?>">

  <fieldset>
    <legend class="text-right">
      <input type="submit" value="save" class="btn btn-success">
      <a href="<?= $listPage ?>" class="btn btn-danger">cancel</a>
    </legend>
    <?php foreach ($form as $field) : ?>
    <div class="form-group">
      <label class="col-md-2 control-label"><?= 'hidden' === $field['type'] ? '' : $field['label'] ?></label>
      <div class="col-md-10">
        <input class="form-control" type="<?= $field['type'] ?>" value="<?= $field['value'] ?>" name="<?= $field['name'] ?>">
      </div>
    </div>
    <?php endforeach ?>
  </fieldset>

</form>