<form class="text-center" method="POST" action="<?= $listPage ?>">
  <h1>are you sure ?</h1>
  <a href="<?= $listPage ?>" class="btn btn-warning">No</a>
  <input type="hidden" name="uuid" value="<?= $uuid ?>">
  <input type="hidden" name="action" value="<?= $action ?>">
  <input type="submit" class="btn btn-success" value="Yes">
</form>